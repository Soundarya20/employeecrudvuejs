import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
    mode:"history",
    routes:[
        {
            path:"/",
            name:"employees",
            alias:"/employees",
            component: () => import("./components/EmployeeList")
        },
        {
          path: "/employees/:id",
          name: "employee-details",
          component: () => import("./components/Employee")
        },
        {
          path: "/add",
          name: "add",
          component: () => import("./components/AddEmployee")
        },
        {
          path: "/update/:id",
          name: "update",
          component: () => import("./components/UpdateEmployee")
        }
        
    ]
})