import http from "../http-common";

class EmployeeService {
  getAll() {
    return http.get("/listall");
  }

  get(id) {
    return http.get(`/byid/${id}`);
  }

  create(data) {
    return http.post("/add", data);
  }

  update(id, data) {
    return http.put(`/update/${id}`, data);
  }

  delete(id) {
    return http.delete(`/delete/${id}`);
  }

}

export default new EmployeeService();